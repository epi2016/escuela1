package bancos.tests;

import bancos.cuentas.GeneradorNumCuenta;
import org.junit.Assert;

public class GeneradorNumCuentaTest {

    @org.junit.Test
    public void generarMultiplesNumeros() {
        int actual = GeneradorNumCuenta.getInstancia().getNumCuentaActual();
        GeneradorNumCuenta.getInstancia().generarNumCuenta();
        GeneradorNumCuenta.getInstancia().generarNumCuenta();
        int ultimaCuentaGenerada = GeneradorNumCuenta.getInstancia().generarNumCuenta();
        Assert.assertEquals(actual + 3, ultimaCuentaGenerada);
    }
}