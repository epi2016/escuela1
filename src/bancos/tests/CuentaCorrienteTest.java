package bancos.tests;

import bancos.cuentas.*;
import org.junit.Assert;

public class CuentaCorrienteTest {

    @org.junit.Test
    public void extraerDinero() throws DNIInvalidoException, SaldoInsuficienteException {
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30100100, 1000, 100, "Juan Perez");
        cuentaCorriente.extraer(500);
        Assert.assertEquals("Saldo real" + cuentaCorriente.getSaldo(),500, cuentaCorriente.getSaldo(), 0.001);
    }

    @org.junit.Test
    public void extraerDineroUsandoGiroEnRojo() throws DNIInvalidoException, SaldoInsuficienteException {
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30100100, 1000, 100, "Juan Perez");
        cuentaCorriente.extraer(1050);
        Assert.assertEquals(50, cuentaCorriente.getGiroEnRojo(), 0.001);
    }

    @org.junit.Test(expected = SaldoInsuficienteException.class)
    public void saldoYGiroEnRojoInsuficiente() throws DNIInvalidoException, SaldoInsuficienteException {
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30100100, 1000, 100, "Juan Perez");
        cuentaCorriente.extraer(300000);
    }

    @org.junit.Test
    public void depositarDinero() throws DNIInvalidoException, DepositoInvalidoException {
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30100100, 1000, 100, "Juan Perez");
        cuentaCorriente.depositar(500);
        Assert.assertEquals(1500, cuentaCorriente.getSaldo(), 0.001);
    }

    @org.junit.Test
    public void depositarDineroYRecuperarSaldoYGiroEnRojo() throws DNIInvalidoException, SaldoInsuficienteException,
            DepositoInvalidoException {
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30100100, 1000, 100, "Juan Perez");
        cuentaCorriente.extraer(1050);
        cuentaCorriente.depositar(150);
        Assert.assertEquals(100, cuentaCorriente.getSaldo(), 0.001);
    }

    @org.junit.Test
    public void depositarDineroYRecuperarGiroEnRojo() throws DNIInvalidoException, SaldoInsuficienteException,
            DepositoInvalidoException {
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30100100, 1000, 100, "Juan Perez");
        cuentaCorriente.extraer(1050);
        cuentaCorriente.depositar(30);
        Assert.assertEquals(80, cuentaCorriente.getGiroEnRojo(), 0.001);
    }

    @org.junit.Test
    public void agregarNombreTitular() throws DNIInvalidoException, NombreInvalidoException {
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30100100, 1000, 100, "Juan Perez");
        cuentaCorriente.agregarTitular("Pedro Rodriguez");
        String nombreNuevoTitular = cuentaCorriente.getListaNombresTitulares().get(1);
        Assert.assertEquals(nombreNuevoTitular, "Pedro Rodriguez");
    }

    @org.junit.Test(expected = NombreInvalidoException.class)
    public void agregarNombreTitularInvalido() throws DNIInvalidoException, NombreInvalidoException {
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30100100, 1000, 100, "Juan Perez");
        cuentaCorriente.agregarTitular("12");
    }

    @org.junit.Test
    public void numeroDeCuentaGenerado() throws DNIInvalidoException {
        int anterior = GeneradorNumCuenta.getInstancia().getNumCuentaActual();
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30200100, 100, 1000, "Juan Perez");
        Assert.assertEquals(anterior + 1, cuentaCorriente.getNumCuenta());
    }

    @org.junit.Test(expected = DepositoInvalidoException.class)
    public void depositoInvalido() throws DNIInvalidoException, DepositoInvalidoException {
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30200100, 100, 1000, "Juan Perez");
        cuentaCorriente.depositar(-4);
    }

    @org.junit.Test(expected = DNIInvalidoException.class)
    public void dniInvalidoPorLongitud() throws DNIInvalidoException {
        CuentaCorriente cuentaCorriente = new CuentaCorriente(10, 20, 10, "Juan Perez");
    }

    @org.junit.Test(expected = DNIInvalidoException.class)
    public void dniNegativoInvalido() throws DNIInvalidoException {
        CuentaCorriente cuentaCorriente = new CuentaCorriente(-30200500, 20, 10, "Juan Perez");
    }
}
