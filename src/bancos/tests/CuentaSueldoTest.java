package bancos.tests;

import bancos.cuentas.*;
import org.junit.Assert;

public class CuentaSueldoTest {

    @org.junit.Test
    public void extraerDinero() throws DNIInvalidoException, NombreInvalidoException, SaldoInsuficienteException {
        CuentaSueldo cuentaSueldo = new CuentaSueldo(38300200, 100, "Juan Perez", "Perez Hermanos");
        cuentaSueldo.extraer(50);
        Assert.assertEquals(50, cuentaSueldo.getSaldo(), 0.001);
    }

    @org.junit.Test
    public void depositarDinero() throws DNIInvalidoException, NombreInvalidoException, DepositoInvalidoException {
        CuentaSueldo cuentaSueldo = new CuentaSueldo(30200100, 100, "Juan Perez", "Perez Hermanos");
        cuentaSueldo.depositar(50);
        Assert.assertEquals(150, cuentaSueldo.getSaldo(), 0.001);
    }

    @org.junit.Test(expected = SaldoInsuficienteException.class)
    public void dineroInsuficiente() throws DNIInvalidoException, NombreInvalidoException, SaldoInsuficienteException {
        CuentaSueldo cuentaSueldo = new CuentaSueldo(38300200, 20, "Juan Perez", "Perez Hermanos");
        cuentaSueldo.extraer(50);
        Assert.assertEquals(50, cuentaSueldo.getSaldo(), 0.001);
    }

    @org.junit.Test(expected = DNIInvalidoException.class)
    public void dniInvalidoPorLongitud() throws DNIInvalidoException, NombreInvalidoException {
        CuentaSueldo cuentaSueldo = new CuentaSueldo(10, 20, "Juan Perez", "Perez Hermanos");
    }

    @org.junit.Test(expected = DNIInvalidoException.class)
    public void dniNegativoInvalido() throws DNIInvalidoException, NombreInvalidoException {
        CuentaSueldo cuentaSueldo = new CuentaSueldo(-40300200, 20, "Juan Perez", "Perez Hermanos");
    }

    @org.junit.Test(expected = NombreInvalidoException.class)
    public void nombreTitularConNumeroInvalido() throws DNIInvalidoException, NombreInvalidoException {
        CuentaSueldo cuentaSueldo = new CuentaSueldo(30200100, 100, "1", "Perez Hermanos");
    }

    @org.junit.Test(expected = NombreInvalidoException.class)
    public void nombreTitularVacioInvalido() throws DNIInvalidoException, NombreInvalidoException {
        CuentaSueldo cuentaSueldo = new CuentaSueldo(30200100, 100, " ", "Perez Hermanos");
    }

    @org.junit.Test(expected = NombreInvalidoException.class)
    public void nombreEmpresaInvalido() throws DNIInvalidoException, NombreInvalidoException {
        CuentaSueldo cuentaSueldo = new CuentaSueldo(30200100, 100, "Juan Perez", " ");
    }

    @org.junit.Test
    public void numeroDeCuentaGenerado() throws DNIInvalidoException, NombreInvalidoException {
        int anterior = GeneradorNumCuenta.getInstancia().getNumCuentaActual();
        CuentaSueldo cuentaSueldo = new CuentaSueldo(30200100, 100, "Juan Perez", "Perez Hermanos");
        Assert.assertEquals(anterior + 1, cuentaSueldo.getNumCuenta());
    }

    @org.junit.Test(expected = DepositoInvalidoException.class)
    public void depositoInvalido() throws DNIInvalidoException, NombreInvalidoException, DepositoInvalidoException {
        CuentaSueldo cuentaSueldo = new CuentaSueldo(30200100, 100, "Juan Perez", "Perez Hermanos");
        cuentaSueldo.depositar(-4);
    }
}
