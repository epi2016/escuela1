package bancos.tests;

import bancos.clientes.Cliente;
import bancos.cuentas.CuentaCorriente;
import bancos.cuentas.CuentaSueldo;
import bancos.cuentas.*;
import org.junit.Assert;

public class ClienteTest {

    @org.junit.Test
    public void agregarCuentaCorriente() throws DNIInvalidoException {
        Cliente cliente = new Cliente();
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30100200, 1000, 100, "Juan Perez");
        cliente.agregarCuentaCorriente(cuentaCorriente);
        Assert.assertTrue(cliente.getCuentasSet().contains(cuentaCorriente));
    }

    @org.junit.Test
    public void agregarCuentaSueldo() throws DNIInvalidoException, NombreInvalidoException,
            NoSePuedeCrearCuentaException {
        Cliente cliente = new Cliente();
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30100200, 1000, 100, "Juan Perez");
        cliente.agregarCuentaCorriente(cuentaCorriente);
        CuentaSueldo cuentaSueldo = new CuentaSueldo(30100200, 1000, "Juan Perez", "Perez Hermanos");
        cliente.agregarCuentaSueldo(cuentaSueldo);
        Assert.assertTrue(cliente.getCuentasSet().contains(cuentaSueldo));
    }

    @org.junit.Test(expected = NoSePuedeCrearCuentaException.class)
    public void noPuedoAgregarMasSueldoQueCorriente() throws NoSePuedeCrearCuentaException, DNIInvalidoException,
            NombreInvalidoException {
        Cliente cliente = new Cliente();
        CuentaSueldo cuentaSueldo = new CuentaSueldo(30100200, 1000, "Juan Perez", "Perez Hermanos");
        cliente.agregarCuentaSueldo(cuentaSueldo);
    }

    @org.junit.Test
    public void noPuedoAgregarCuentasCorrientesDuplicadas() throws DNIInvalidoException, NombreInvalidoException,
            NoSePuedeCrearCuentaException {
        Cliente cliente = new Cliente();
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30100200, 1000, 100, "Juan Perez");
        cliente.agregarCuentaCorriente(cuentaCorriente);
        cliente.agregarCuentaCorriente(cuentaCorriente);
        Assert.assertEquals(1, cliente.getCuentasSet().size());
    }

    @org.junit.Test
    public void noPuedoAgregarCuentasSueldoDuplicadas() throws DNIInvalidoException, NombreInvalidoException,
            NoSePuedeCrearCuentaException {
        Cliente cliente = new Cliente();
        CuentaCorriente cuentaCorriente = new CuentaCorriente(30100200, 1000, 100, "Juan Perez");
        CuentaCorriente cuentaCorriente2 = new CuentaCorriente(60200500, 1000, 100, "Pedro Rodriguez");
        cliente.agregarCuentaCorriente(cuentaCorriente);
        cliente.agregarCuentaCorriente(cuentaCorriente2);
        CuentaSueldo cuentaSueldo = new CuentaSueldo(30100200, 1000, "Juan Perez", "Perez Hermanos");
        cliente.agregarCuentaSueldo(cuentaSueldo);
        cliente.agregarCuentaSueldo(cuentaSueldo);
        Assert.assertEquals(3, cliente.getCuentasSet().size());
    }
}
