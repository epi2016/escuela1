package bancos.cuentas;

/**
 * Clase que aplica el patrón de diseño Singleton para generar múltiples números de cuentas distintos
 */
public class GeneradorNumCuenta {

    private static GeneradorNumCuenta instancia;
    private int numCuenta;

    private GeneradorNumCuenta() {
        this.numCuenta = 0;
    }

    public static GeneradorNumCuenta getInstancia() {
        if (instancia == null) {
            instancia = new GeneradorNumCuenta();
        }
        return instancia;
    }

    public int generarNumCuenta() {
        return (numCuenta += 1);
    }

    public int getNumCuentaActual() {
        return numCuenta;
    }
}