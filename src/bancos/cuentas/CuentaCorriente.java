package bancos.cuentas;

import java.util.ArrayList;
import java.util.List;

public class CuentaCorriente extends Cuenta {

    private final List<String> listaNombresTitulares;
    private final double creditoMaximo;
    private double giroEnRojo;

    public CuentaCorriente(int dni, double saldo, double giroEnRojo, String nombreTitular) throws DNIInvalidoException {
        super(dni, saldo);
        this.giroEnRojo = giroEnRojo;
        this.listaNombresTitulares = new ArrayList<String>();
        this.listaNombresTitulares.add(nombreTitular);
        this.creditoMaximo = giroEnRojo;
    }

    /**
     * Depositar dinero en la cuenta.
     * Recupera el giro en rojo en caso de ser menor al monto de crédito máximo fijado para la cuenta y deposita el
     * resto en la cuenta.
     *
     * @param deposito
     * @throws DepositoInvalidoException
     */
    @Override
    public void depositar(double deposito) throws DepositoInvalidoException {
        if (getGiroEnRojo() < getCreditoMaximo()) {
            if (deposito <= (getCreditoMaximo() - getGiroEnRojo())) {
                setGiroEnRojo(getGiroEnRojo() + deposito);
            } else {
                double diferencia = getCreditoMaximo() - getGiroEnRojo();
                setGiroEnRojo(getGiroEnRojo() + diferencia);
                super.depositar(deposito - diferencia);
            }
        } else {
            super.depositar(deposito);
        }
    }

    /**
     * Extrae dinero de la cuenta.
     * En caso de no tener suficiente saldo disponible en la cuenta, usa el crédito disponible.
     *
     * @param extraccion
     * @throws SaldoInsuficienteException
     */
    @Override
    public void extraer(double extraccion) throws SaldoInsuficienteException {
        if (extraccion > getSaldo() + getGiroEnRojo()) {
            throw new SaldoInsuficienteException("El saldo de la cuenta no es suficiente");
        } else if (extraccion > getSaldo()) {
            double diferencia = extraccion - getSaldo();
            extraer(extraccion - diferencia);
            setGiroEnRojo(getGiroEnRojo() - diferencia);
        } else {
            super.extraer(extraccion);
        }
    }

    public double getGiroEnRojo() {
        return giroEnRojo;
    }

    public void setGiroEnRojo(double giroEnRojo) {
        if (giroEnRojo < getCreditoMaximo()) {
            this.giroEnRojo = giroEnRojo;
        }
    }

    public List<String> getListaNombresTitulares() {
        return listaNombresTitulares;
    }

    public void agregarTitular(String nombreTitular) throws NombreInvalidoException {
        if (Utilities.validateNombreTitular(nombreTitular)) {
            throw new NombreInvalidoException("El nombre no puede estar vacio o contener digitos");
        }
        getListaNombresTitulares().add(nombreTitular);
    }

    public double getCreditoMaximo() {
        return creditoMaximo;
    }
}