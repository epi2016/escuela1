package bancos.cuentas;

public class DNIInvalidoException extends Exception {

    public DNIInvalidoException(String s) {
        super(s);
    }
}