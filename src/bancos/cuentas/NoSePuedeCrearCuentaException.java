package bancos.cuentas;

public class NoSePuedeCrearCuentaException extends Exception {

    public NoSePuedeCrearCuentaException(String s) {
        super(s);
    }
}
