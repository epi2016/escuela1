package bancos.cuentas;

public class CuentaSueldo extends Cuenta {

    private final String nombreTitular;
    private String nombreEmpresa;

    public CuentaSueldo(int dni, double saldo, String nombreTitular, String nombreEmpresa) throws DNIInvalidoException,
            NombreInvalidoException {
        super(dni, saldo);
        if (Utilities.validateNombreEmpresa(nombreEmpresa) || Utilities.validateNombreTitular(nombreTitular)) {
            throw new NombreInvalidoException("El nombre no puede ser vacio. " +
                    "El nombre de titular no puede contener numeros");
        }
        this.nombreTitular = nombreTitular;
        this.nombreEmpresa = nombreEmpresa;
    }



    @Override
    public void extraer(double extraccion) throws SaldoInsuficienteException {
        if (getSaldo() - extraccion < 0) {
            throw new SaldoInsuficienteException("El saldo de la cuenta no es suficiente");
        }
        super.extraer(extraccion);
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }
}
