package bancos.cuentas;

public class NombreInvalidoException extends Exception {

    public NombreInvalidoException(String s) {
        super(s);
    }
}
