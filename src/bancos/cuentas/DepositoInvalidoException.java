package bancos.cuentas;

public class DepositoInvalidoException extends Exception {

    public DepositoInvalidoException(String s) {
        super(s);
    }
}
