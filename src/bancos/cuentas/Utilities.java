package bancos.cuentas;

public final class Utilities {

    public static boolean validateNombreTitular(String nombreTitular) {
        return nombreTitular.trim().isEmpty() || nombreTitular.trim().matches(".*\\d.*");
    }

    public static boolean validateNombreEmpresa(String nombreEmpresa) {
        return nombreEmpresa.trim().isEmpty();
    }

    public static boolean validateDni(int dni) {
        return dni < 0 || String.valueOf(dni).length() < 7;
    }
}
