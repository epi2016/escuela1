package bancos.cuentas;

public abstract class Cuenta {

    private final int numCuenta;
    private final int dni;
    private double saldo;

    public Cuenta(int dni, double saldo) throws DNIInvalidoException {
        if (Utilities.validateDni(dni)) {
            throw new DNIInvalidoException("El DNI no tiene un formato valido");
        }
        this.numCuenta = GeneradorNumCuenta.getInstancia().generarNumCuenta();
        this.dni = dni;
        this.saldo = saldo;
    }



    public void depositar(double deposito) throws DepositoInvalidoException {
        if (deposito <= 0) {
            throw new DepositoInvalidoException("Debe depositar mas que $0");
        }
        this.saldo += deposito;
    }

    public  void extraer(double extraccion) throws SaldoInsuficienteException {
        this.saldo -= extraccion;
    }

    public final int getNumCuenta() {
        return numCuenta;
    }

    public final int getDni() {
        return dni;
    }

    public final double getSaldo() {
        return saldo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cuenta cuenta = (Cuenta) o;

        if (numCuenta != cuenta.numCuenta) return false;
        return dni == cuenta.dni;
    }

    @Override
    public int hashCode() {
        int result = numCuenta;
        result = 31 * result + dni;
        return result;
    }
}
