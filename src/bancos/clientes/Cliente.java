package bancos.clientes;

import bancos.cuentas.Cuenta;
import bancos.cuentas.CuentaCorriente;
import bancos.cuentas.CuentaSueldo;
import bancos.cuentas.NoSePuedeCrearCuentaException;

import java.util.HashSet;
import java.util.Set;

public class Cliente {

    private final Set<Cuenta> cuentasSet;
    private int cantidadCuentaCorriente;
    private int cantidadCuentaSueldo;

    public Cliente() {
        this.cuentasSet = new HashSet<>();
        this.cantidadCuentaCorriente = 0;
        this.cantidadCuentaSueldo = 0;
    }

    public void agregarCuentaCorriente(CuentaCorriente cuentaCorriente) {
        if (getCuentasSet().add(cuentaCorriente)) {
            this.cantidadCuentaCorriente += 1;
        }
    }

    public void agregarCuentaSueldo(CuentaSueldo cuentaSueldo) throws NoSePuedeCrearCuentaException {
        if (this.cantidadCuentaSueldo + 1 > this.cantidadCuentaCorriente) {
            throw new NoSePuedeCrearCuentaException("No se puede tener mas cuentas sueldo que corrientes");
        } else {
            if (getCuentasSet().add(cuentaSueldo)) {
                this.cantidadCuentaSueldo += 1;
            }
        }
    }

    public Set<Cuenta> getCuentasSet() {
        return cuentasSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cliente cliente = (Cliente) o;

        return cuentasSet != null ? cuentasSet.equals(cliente.cuentasSet) : cliente.cuentasSet == null;

    }

    @Override
    public int hashCode() {
        return cuentasSet != null ? cuentasSet.hashCode() : 0;
    }
}
